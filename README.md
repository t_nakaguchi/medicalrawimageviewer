# Medical Raw Image Viewer プロジェクト
- 医用画像RAWデータ表示ソフトウェア

### 開発環境
- Visual C++ 2019
- OpenCV 4.2.0
- GStreamer 1.16.2
- TBB
- cvui 2.7.0
