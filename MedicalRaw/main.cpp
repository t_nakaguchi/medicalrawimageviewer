#include "myOpenCV.h"
#define CVUI_IMPLEMENTATION
#include "cvui.h"
#include <iostream>
#include <Windows.h>

const cv::String WIN_MAIN = "Medical Raw Image Viewer";
const cv::Size MAIN_FRAME_SIZE = cv::Size(702, 532);
const int KEY_ESC = 27;
const int IMAGE_SIZE = 512;

// 画像データクラス
class RawData {
private:
	int _numSlices;			// スライス数
	int _width, _height;	// 画像の幅と高さ
	SHORT* _volume;			// RAW画像データ
	BYTE* _mask;			// マスクデータ

public:
	// コンストラクタ
	RawData() {
		_numSlices = 0;
		_width = _height = IMAGE_SIZE;
		_volume = NULL;
		_mask = NULL;
	}

	// デストラクタ
	~RawData() {
		if (_volume) delete[] _volume;
		if (_mask) delete[] _mask;
	}

	// スライス数を返す
	int NumSlices() { 
		return _numSlices; 
	}

	// スライスの画像サイズを返す
	cv::Size GetSize() { 
		return cv::Size(_width, _height);
	}

	// 指定スライスの画像ポインタを取得
	SHORT* SliceImg(int slice) { 
		if (slice < 0 || slice >= _numSlices) return NULL;
		return _volume + _width * _height * slice;
	}

	// 指定スライスのマスクポインタを取得
	BYTE* SliceMask(int slice) {
		if (slice < 0 || slice >= _numSlices) return NULL;
		return _mask + _width * _height * slice;
	}

	// マスクが読み込まれているか
	bool IsMaskLoaded() {
		return _mask != NULL;
	}

	// RAW画像を読み込む
	bool LoadVolume(const char* filename) {
		std::fstream ifs(filename, std::ios::binary | std::ios::in);
		if (!ifs) { 
			std::cerr << "ファイルが開けませんでした．" << filename << std::endl;
			return false; 
		}

		// ファイルサイズ取得
		ifs.seekg(0, std::ios::end);
		size_t rawSize = ifs.tellg();
		//std::cout << "size is " << rawSize << "bytes." << std::endl;

		// スライス数取得
		_numSlices = (int)rawSize / (_width * _height * 2);
		if (_numSlices * _width * _height * 2 != (int)rawSize) {
			std::cerr << "ファイルサイズが正しくありません．" << std::endl;
			_numSlices = 0;
			return false;
		}

		// メモリ初期化
		if (_volume) {
			delete[] _volume;
			_volume = NULL;
		}
		if (_mask) {
			delete[] _mask;
			_mask = NULL;
		}

		// データ読み込み
		ifs.seekg(0, std::ios::beg);
		_volume = new SHORT[rawSize / 2];
		ifs.read((char*)_volume, rawSize);
		ifs.close();
		return true;
	}

	// マスクを読み込む
	bool LoadMask(const char* filename) {
		std::fstream ifs(filename, std::ios::binary | std::ios::in);
		if (!ifs) {
			std::cerr << "ファイルが開けませんでした．" << filename << std::endl;
			return false;
		}

		// ファイルサイズ取得
		ifs.seekg(0, std::ios::end);
		size_t rawSize = ifs.tellg();
		//std::cout << "size is " << rawSize << "bytes." << std::endl;

		// スライス数の確認
		int slices = (int)rawSize / (_width * _height);
		if (_numSlices != slices) {
			std::cerr << "スライス数が一致しません．" << std::endl;
			return false;
		}

		// メモリ初期化
		if (_mask) {
			delete[] _mask;
			_mask = NULL;
		}

		// データ読み込み
		ifs.seekg(0, std::ios::beg);
		_mask = new BYTE[rawSize];
		ifs.read((char*)_mask, rawSize);
		ifs.close();
		return true;
	}
};

// ファイルダイアログ表示
// こちらから拝借 http://www14.big.or.jp/~ken1/tech/tech14.html
BOOL FileDialog(HWND hwnd, char mode, char* path, char* name, const char* title, const char* filter)
{
	//文字列にヌル文字を代入しておく
	memset(path, '\0', sizeof path);
	memset(name, '\0', sizeof name);

	OPENFILENAME ofn;
	memset(&ofn, 0, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = path;     //選択されたファイル名を受け取る(フルパス)
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrFileTitle = name;//選択されたファイル名
	ofn.nMaxFileTitle = MAX_PATH;

	ofn.lpstrFilter = filter;//フィルタ
	ofn.lpstrTitle = title;//ダイアログボックスのタイトル

	if (mode == 'g')
	{
		ofn.Flags = OFN_FILEMUSTEXIST;
		if (GetOpenFileName(&ofn) == FALSE)
			return FALSE;
	} else if (mode == 's')
	{
		ofn.Flags = OFN_OVERWRITEPROMPT;
		if (GetSaveFileName(&ofn) == FALSE)
			return FALSE;
	} else
		return FALSE;

	return TRUE;
}

// ウインドウ処理
cv::Mat WindowProcess(RawData& rd, int slice, int windowCenter, int windowWidth) {

	// 画像取得
	cv::Mat img(rd.GetSize(), CV_16SC1, rd.SliceImg(slice));
	cv::Mat disp;

	// Window処理
	// windowMin = windowCenter - windowWidth / 2
	// Idisp = (Iorg - windowMin) * 255 / windowWidth
	//       = (255 / windowWidth) * Iorg + (-windowMin * 255 /  windowWidth)
	double windowMin = windowCenter - windowWidth / 2.0;
	double alpha = 255. / windowWidth;
	double beta = -windowMin * 255. / windowWidth;
	img.convertTo(disp, CV_8UC1, alpha, beta);

	// 1ch（モノクロ）から3ch（カラー）へデータフォーマット変換
	cv::cvtColor(disp, disp, cv::COLOR_GRAY2BGR);
	return disp;
}

// メイン関数
int main() {
	// 表示パラメータ初期化
	RawData rd;
	int slice = 0;
	int windowCenter = 0;
	int windowWidth = 300;
	float maskIntensity = 0.8F;

	// ウインドウ初期化
	cvui::init(WIN_MAIN);
	cv::Mat mainFrame(MAIN_FRAME_SIZE, CV_8UC3);

	// メインループ
	while (cv::getWindowProperty(WIN_MAIN, 0) >= 0) {
		// メインウインドウフレームクリア
		mainFrame = cv::Scalar(49, 52, 49);

		// GUI
		cvui::beginRow(mainFrame, 10, 10, -1, -1, 0);

		// 操作部
		cvui::beginColumn(140, -1, 0);
		if (cvui::button(140, 30, "Open Raw Image")) {
			char path[MAX_PATH], filename[MAX_PATH];
			if (FileDialog(NULL, 'g', path, filename, "RAW画像ファイルを開く", "raw(*.raw)\0*.raw\0\0")) {
				rd.LoadVolume(path);
			}
		}
		if (rd.NumSlices() > 0) {
			cvui::space(20);
			if (cvui::button(140, 30, "Open Mask")) {
				char path[MAX_PATH], filename[MAX_PATH];
				if (FileDialog(NULL, 'g', path, filename, "マスクファイルを開く", "raw(*.raw)\0*.raw\0\0")) {
					rd.LoadMask(path);
				}
			}
			cvui::space(20);
			cvui::text("Slice No.");
			cvui::trackbar(140, &slice, 0, rd.NumSlices()-1, 1, "%.0Lf");
			cvui::space(20);
			cvui::text("Window Center");
			cvui::trackbar(140, &windowCenter, -1000, 1000, 1, "%.0Lf");
			cvui::space(20);
			cvui::text("Window Width");
			cvui::trackbar(140, &windowWidth, 1, 1000, 1, "%.0Lf");
			if (rd.IsMaskLoaded()) {
				cvui::space(20);
				cvui::text("Mask Intensity");
				cvui::trackbar(140, &maskIntensity, 0.F, 1.0F, 1, "%.2Lf");
			}
			cvui::space(20);
			if (cvui::button(140, 30, "Save to PNG Files")) {
				char path[MAX_PATH], filename[MAX_PATH], base[MAX_PATH], pathout[MAX_PATH];
				if (FileDialog(NULL, 's', path, filename, "PNG保存ファイルを開く", "png(*.png)\0*.png\0\0")) {
					// 保存ファイル名の拡張子の前に連番付与
					char* p = strstr(path, ".png");
					if (p != NULL) {
						strncpy_s(base, MAX_PATH, path, p - path);
					} else {
						strcpy_s(base, MAX_PATH, path);
					}

					// 全フレームのPNG出力
					for (int sl = 0; sl < rd.NumSlices(); sl++) {
						sprintf_s(pathout, MAX_PATH, "%s_%04d.png", base, sl);
						cv::Mat disp = WindowProcess(rd, sl, windowCenter, windowWidth);
						cv::imwrite(pathout, disp);
						std::cout << "Write: " << pathout << std::endl;
					}
				}
			}
		}
		cvui::endColumn();

		// 画像表示部
		if (rd.NumSlices() > 0) {
			cvui::space(30);
			cv::Mat disp = WindowProcess(rd, slice, windowCenter, windowWidth);

			if (rd.IsMaskLoaded()) {
				cv::Mat mask(rd.GetSize(), CV_8UC1, rd.SliceMask(slice));
				cv::Mat color(rd.GetSize(), CV_8UC3, CV_RGB(255, 0, 0));
				cv::Mat mixed;
				cv::addWeighted(disp, 1.0 - maskIntensity, color, maskIntensity, 0, mixed);
				mixed.copyTo(disp, mask);
			}

			cvui::image(disp);
		}
		cvui::endRow();

		// 表示
		cvui::update();
		cv::imshow(WIN_MAIN, mainFrame);

		// メッセージ処理
		if (cv::waitKey(1) == KEY_ESC) break;
	}

	return 0;
}
